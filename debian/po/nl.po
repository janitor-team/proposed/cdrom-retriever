# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of nl.po to Dutch
# Dutch messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Frans Pop <aragorn@tiscali.nl>, 2005.
# Frans Pop <elendil@planet.nl>, 2007, 2008, 2009, 2010.
# Eric Spreen <erispre@gmail.com>, 2010.
# Jeroen Schot <schot@a-eskwadraat.nl>, 2011, 2012.
#
# Translations from iso-codes:
# Translations taken from ICU SVN on 2007-09-09.
# Tobias Toedter <t.toedter@gmx.net>, 2007.
#
# Elros Cyriatan <cyriatan@fastmail.fm>, 2004.
# Luk Claes <luk.claes@ugent.be>, 2005.
# Freek de Kruijf <f.de.kruijf@hetnet.nl>, 2006, 2007, 2008, 2009, 2010, 2011.
# Taco Witte <tcwitte@cs.uu.nl>, 2004.
# Reinout van Schouwen <reinouts@gnome.org>, 2007.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2014, 2015, 2016, 2017, 2018, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer/sublevel1\n"
"Report-Msgid-Bugs-To: cdrom-retriever@packages.debian.org\n"
"POT-Creation-Date: 2019-09-29 17:12+0000\n"
"PO-Revision-Date: 2019-11-06 16:14+0100\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl1:
#: ../load-cdrom.templates:1001
msgid "Load installer components from installation media"
msgstr "Componenten van het installatiesysteem laden uit de installatiemedia"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-retriever.templates:1001
msgid "Failed to copy file from installation media. Retry?"
msgstr ""
"Bestand van installatiemedia kopiëren is mislukt. Wilt u opnieuw proberen?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-retriever.templates:1001
msgid ""
"There was a problem reading data. Please make sure you have inserted the "
"installation media correctly. If retrying does not work, you should check "
"the integrity of your installation media (there is an associated entry in "
"the main menu for that)."
msgstr ""
"Er was een probleem bij het lezen van de gegevens. Zorg ervoor dat u de "
"installatiemedia correct geplaatst heeft. Als opnieuw proberen niet werkt, "
"zou u het beste uw installatiemedia op fouten controleren (er is daarvoor in "
"het hoofdmenu een bijbehorend item)."
