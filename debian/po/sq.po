# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Albanian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
#
#
# Translations from iso-codes:
#   Alastair McKinstry <mckinstry@debian.org>, 2004
#   Elian Myftiu <elian.myftiu@gmail.com>, 2004,2006.
#
# Eva Vranici <evavranici@gmail.com>, 2017.
# Silva Arapi <silva.arapi@gmail.com>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: cdrom-retriever@packages.debian.org\n"
"POT-Creation-Date: 2019-09-29 17:12+0000\n"
"PO-Revision-Date: 2017-09-11 23:13+0300\n"
"Last-Translator: Sotirios Vrachas <sotirios@vrachas.net>\n"
"Language-Team: Albanian <debian-l10n-albanian@lists.debian.org>\n"
"Language: sq\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n<11 ? 3 : "
"4;\n"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl1:
#: ../load-cdrom.templates:1001
#, fuzzy
msgid "Load installer components from installation media"
msgstr "Ngarko përbërësit nga instaluesi ISO"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-retriever.templates:1001
#, fuzzy
msgid "Failed to copy file from installation media. Retry?"
msgstr "Dështoi në kopjimin e kartelës nga CDROM. Ta riprovoj?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-retriever.templates:1001
#, fuzzy
msgid ""
"There was a problem reading data. Please make sure you have inserted the "
"installation media correctly. If retrying does not work, you should check "
"the integrity of your installation media (there is an associated entry in "
"the main menu for that)."
msgstr ""
"Pati një problem me leximin e të dhënave nga CDROM. Të lutem sigurohu që ai "
"gjendet në lexues. Nëse ripërpjekja nuk ka sukses, duhet të kontrollosh "
"integritetin e CDROM-it tënd."
