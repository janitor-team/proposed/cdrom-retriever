# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Japanese messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
#
# Translations from iso-codes:
#   Alastair McKinstry <mckinstry@computer.org>, 2001, 2002.
#   Free Software Foundation, Inc., 2000, 2001, 2004, 2005, 2006
#   IIDA Yosiaki <iida@gnu.org>, 2004, 2005, 2006.
#   Kenshi Muto <kmuto@debian.org>, 2006-2018
#   Takayuki KUSANO <AE5T-KSN@asahi-net.or.jp>, 2001.
#   Takuro Ashie <ashie@homa.ne.jp>, 2001.
#   Tobias Quathamer <toddy@debian.org>, 2007.
#     Translations taken from ICU SVN on 2007-09-09
#   Translations from KDE: 
#   - Taiki Komoda <kom@kde.gr.jp>
#   Yasuaki Taniguchi <yasuakit@gmail.com>, 2010, 2011.
#   Yukihiro Nakai <nakai@gnome.gr.jp>, 2000.
#
#   Kentaro Hayashi <kenhys@gmail.com>, 2021.
#   YOSHINO Yoshihito <yy.y.ja.jp@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: cdrom-retriever@packages.debian.org\n"
"POT-Creation-Date: 2019-09-29 17:12+0000\n"
"PO-Revision-Date: 2021-05-06 03:32+0000\n"
"Last-Translator: Nozomu KURASAWA <nabetaro@caldron.jp>\n"
"Language-Team: Debian L10n Japanese <debian-japanese@lists.debian.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl1:
#: ../load-cdrom.templates:1001
msgid "Load installer components from installation media"
msgstr "インストールメディアからインストーラコンポーネントをロード"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-retriever.templates:1001
msgid "Failed to copy file from installation media. Retry?"
msgstr ""
"インストールメディアからのファイルのコピーに失敗しました。再試行しますか?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-retriever.templates:1001
msgid ""
"There was a problem reading data. Please make sure you have inserted the "
"installation media correctly. If retrying does not work, you should check "
"the integrity of your installation media (there is an associated entry in "
"the main menu for that)."
msgstr ""
"データの読み込み中に問題が発生しました。インストールメディアが正しく挿入され"
"ていることを確認してください。再試行でうまくいかないようであれば、あなたのイ"
"ンストールメディアの完全性をチェックしてください (メインメニューにそのための"
"関連するエントリがあります)。"
